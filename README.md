# Sampleproject
Tento projekt slouží pouze jako ukázka.
## Součásti projektu
Součástí projektu je databáze MariaDB uložená v Dockeru ve složce ..\sampleproject\WebTemplateAPI\Docker ta je potřeba pustit pro správný chod aplikace. Dále se aplikace skládá z ASP.NET Core 6 backendu a frontendu v Angularu 14.
## Funkce
!!POZOR, BEZ DATABÁZE NENÍ ŽÁDNÁ ČÁST APLIKACE FUNKČNÍ!!

Projekt je postaven na několika základních funkcích. V první řadě jde o registraci a přihlášení nového uživatele za pomocí Microsoft.AspNetCore.Authentication.JwtBearer a Microsoft.AspNetCore.Identity.EntityFrameworkCore a Angular-JWT. Další funkční část je jádro celé aplikace, které dovolí přihlášenému uživateli vytvářet příspěvky, komentáře a odpovědi na ně.
