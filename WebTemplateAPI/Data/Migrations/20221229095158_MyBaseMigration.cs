﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebTemplateAPI.Data.Migrations
{
    public partial class MyBaseMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "reports_handled");

            migrationBuilder.DropTable(
                name: "reports");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "reports",
                columns: table => new
                {
                    reports_id = table.Column<decimal>(type: "decimal(65,30)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    reportedpost = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    UserId = table.Column<string>(type: "varchar(95)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    reportedby = table.Column<string>(type: "varchar(95)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    reporttext = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reports", x => x.reports_id);
                    table.ForeignKey(
                        name: "FK_reports_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_reports_posts_reportedpost",
                        column: x => x.reportedpost,
                        principalTable: "posts",
                        principalColumn: "posts_id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "reports_handled",
                columns: table => new
                {
                    handled_id = table.Column<decimal>(type: "decimal(65,30)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    reports_id = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    UserId = table.Column<string>(type: "varchar(95)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    handledby = table.Column<string>(type: "varchar(95)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    handledstat = table.Column<string>(type: "varchar(20)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reports_handled", x => x.handled_id);
                    table.ForeignKey(
                        name: "FK_reports_handled_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_reports_handled_reports_reports_id",
                        column: x => x.reports_id,
                        principalTable: "reports",
                        principalColumn: "reports_id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_reports_reportedby",
                table: "reports",
                column: "reportedby");

            migrationBuilder.CreateIndex(
                name: "IX_reports_reportedpost",
                table: "reports",
                column: "reportedpost");

            migrationBuilder.CreateIndex(
                name: "IX_reports_reports_id",
                table: "reports",
                column: "reports_id");

            migrationBuilder.CreateIndex(
                name: "IX_reports_reporttext",
                table: "reports",
                column: "reporttext");

            migrationBuilder.CreateIndex(
                name: "IX_reports_UserId",
                table: "reports",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_reports_handled_handled_id",
                table: "reports_handled",
                column: "handled_id");

            migrationBuilder.CreateIndex(
                name: "IX_reports_handled_handledby",
                table: "reports_handled",
                column: "handledby");

            migrationBuilder.CreateIndex(
                name: "IX_reports_handled_handledstat",
                table: "reports_handled",
                column: "handledstat");

            migrationBuilder.CreateIndex(
                name: "IX_reports_handled_reports_id",
                table: "reports_handled",
                column: "reports_id");

            migrationBuilder.CreateIndex(
                name: "IX_reports_handled_UserId",
                table: "reports_handled",
                column: "UserId");
        }
    }
}
