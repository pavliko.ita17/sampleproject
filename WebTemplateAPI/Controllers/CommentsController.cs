﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;
using WebTemplateAPI.Modules;
using WebTemplateAPI.Modules.Data;

namespace WebTemplateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CommentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Comments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comments>>> GetComments()
        {
            return await _context.Comments.ToListAsync();
        }

        // GET: api/Comments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comments>> GetComments(int id)
        {
            var comments = await _context.Comments.FindAsync(id);

            if (comments == null)
            {
                return NotFound();
            }

            return comments;
        }

        // PUT: api/Comments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComments(int id, Comments comments)
        {
            if (id != comments.comm_id)
            {
                return BadRequest();
            }

            _context.Entry(comments).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommentsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Comments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Comments>> PostComments(Comments comments)
        {
            _context.Comments.Add(comments);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComments", new { id = comments.comm_id }, comments);
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComments(int id)
        {
            var comments = await _context.Comments.FindAsync(id);
            if (comments == null)
            {
                return NotFound();
            }

            _context.Comments.Remove(comments);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpGet("LoadComments")]
        public async Task<List<Comments>> LoadComments (int post_id)
        {
            var comments = _context.Comments.Where(x=> x.posts_id == post_id).Where(x=> x.parent_id == 0).ToList();
            return comments;
        }
        [HttpGet("LoadReplies")]
        public async Task<List<Comments>> LoadReplies(int parent_id)
        {
            var comments = _context.Comments.Where(x => x.parent_id == parent_id).ToList();
            return comments;
        }
        public async Task<Users> getAuthor(int comment_id)
        {
            var comments = await _context.Comments.FindAsync(comment_id);
            var author = await _context.Users.FindAsync(comments.author);
            return author;
        }
        private bool CommentsExists(int id)
        {
            return _context.Comments.Any(e => e.comm_id == id);
        }
        [HttpPost("SaveComment")]
        public async Task<SaveCommentResult> SaveComment([FromBody] SaveCommentRequest commentToSave)
        {
            if(commentToSave.Content != "")
            {
                int newId;
                try
                {
                     newId = _context.Comments.Max(u => u.comm_id) + 1;
                }
                catch (Exception)
                {
                    newId = 1;
                }
                var comment = new Comments
                {
                    comm_id = newId,
                    content = commentToSave.Content,
                    author = commentToSave.Author,
                    parent_id = commentToSave.ParentId,
                    posts_id = commentToSave.PostId
                };
                _context.Comments.Add(comment);
                await _context.SaveChangesAsync();
                return new SaveCommentResult { message = "ok", commentId = comment.comm_id };
            }
            else
            {
                return new SaveCommentResult { message = "No content", commentId = 0 };
            }

        }
    }
}
