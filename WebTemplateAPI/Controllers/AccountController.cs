﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.CodeAnalysis.CSharp;
using NuGet.Protocol.Plugins;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using WebTemplateAPI.Modules;
using WebTemplateAPI.Modules.Data;

namespace WebTemplateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Users> userManager;     
        private readonly JwtHandler jwtHandler;
        public AccountController(ApplicationDbContext context, UserManager<Users> userManager, JwtHandler jwtHandler)
        {
            _context = context;
            this.userManager = userManager;
            this.jwtHandler = jwtHandler;
        }
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            var user = await userManager.FindByNameAsync(loginRequest.UserName);
            if (user == null || !await userManager.CheckPasswordAsync(user, loginRequest.Password))
            {
                return Unauthorized(new LoginResult()
                {
                    Success = true,
                    Message = "Invalid Email or Password"
                });
            }
            var secToken = await jwtHandler.GetTokenAsync(user);
            var jwt = new JwtSecurityTokenHandler().WriteToken(secToken);
            Console.WriteLine(jwt);
            return Ok(new LoginResult()
            {
                Success = true,
                Message = "Login Successfull",
                Token = jwt,
            });
        }

        [HttpPost("Registration")]
        public async Task<IActionResult> RegisterUser([FromBody] RegistrationRequest registrationRequest)
        {
            if(registrationRequest==null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            var user = new Users
            {
                UserName = registrationRequest.username,
                Email = registrationRequest.Email
            };
            var result = await userManager.CreateAsync(user, registrationRequest.Password);
            if (!result.Succeeded)
            {
                var errors = result.Errors.Select(e => e.Description);
                return BadRequest(new RegistrationResponse { Errors = errors });
            }
            return Ok(result);
        }
        [HttpPost("GetUId")]
        public async Task<IActionResult> GetUID(GetUserIdRequest usernm)
        {
            if (usernm == null)
            {
                return null;
            }
            var user = await userManager.FindByNameAsync(usernm.username);
            if (user == null)
            {
                return null;
            }
            return Ok(new GetUserIdResponse
            {
                UserId = user.Id
            });
        }
        [HttpGet("GetUserName")]
        public async Task<IActionResult> GetUserName(string userId)
        {
            if(userId == null)
            {
                return null;
            }
            else
            {
                var user = await userManager.FindByIdAsync(userId);
                return Ok(new UserNameResponse
                {
                    UserName = user.UserName
                });
            }
        }
        [HttpPost("CheckPassword")]
        public async Task<CheckPasswordResponse> CheckPassword([FromBody] CheckPasswordRequest check)
        {
            var user = await userManager.FindByNameAsync(check.userName);
            if(!await userManager.CheckPasswordAsync(user, check.password))
            {
                return (new CheckPasswordResponse { message = "Nesprávné jméno nebo heslo" });
            }
            else
            {
                return (new CheckPasswordResponse { message = "" });
            }
        }
    }
}
