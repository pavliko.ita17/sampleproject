﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebTemplateAPI.Modules;
using WebTemplateAPI.Modules.Data;

namespace WebTemplateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Posts>>> GetPosts()
        {
            return await _context.Posts.ToListAsync();
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Posts>> GetPosts(int id)
        {
            var posts = await _context.Posts.FindAsync(id);

            if (posts == null)
            {
                return NotFound();
            }

            return posts;
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosts(int id, Posts posts)
        {
            if (id != posts.posts_id)
            {
                return BadRequest();
            }

            _context.Entry(posts).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Posts>> PostPosts(Posts posts)
        {
            _context.Posts.Add(posts);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosts", new { id = posts.posts_id }, posts);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosts(int id)
        {
            var posts = await _context.Posts.FindAsync(id);
            if (posts == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(posts);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PostsExists(int id)
        {
            return _context.Posts.Any(e => e.posts_id == id);
        }

        [HttpGet("LoadPosts")]
        public async Task<List<Posts>> LoadPosts(int pageNumber,int lastId = 0)
        {   
            if(lastId == 0)
            {
               lastId = _context.Posts.Max(e => e.posts_id);
            }
            int lastPageId = lastId - pageNumber;
            var postss = _context.Posts.Where(x => x.posts_id >= lastPageId).Where(x => x.posts_id <= lastId).ToList();
            return postss;
        }
        [HttpPost("SavePost")]
        public async Task<SavePostResponse> SavePost([FromBody] SavePostRequest savePost)
        {
            if(savePost.content != "")
            {
                int newId;
                try
                {
                    newId = _context.Comments.Max(u => u.comm_id) + 1;
                }
                catch (Exception)
                {
                    newId = 1;
                }

                var Posts = new Posts
                {
                    posts_id = newId,
                    content = savePost.content,
                    created_at = DateTime.Now,
                    author = savePost.author
                };
                _context.Posts.Add(Posts);
                await _context.SaveChangesAsync();
                return (new SavePostResponse { message = "ok", PostId = Posts.posts_id });
            }
            else
            {
                return (new SavePostResponse { message = "No content", PostId = 0 });
            }
        }

    }
}
