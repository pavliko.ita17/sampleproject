﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebTemplateAPI.Modules.Data;
namespace WebTemplateAPI.Modules
{
    public class ApplicationDbContext : IdentityDbContext<Users>
    {
        public ApplicationDbContext() : base()
        {

        }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Users> Users => Set<Users>();
        public DbSet<Posts> Posts => Set<Posts>();
        public DbSet<Comments> Comments => Set<Comments>();
    }
}
