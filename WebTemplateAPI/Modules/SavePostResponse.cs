﻿namespace WebTemplateAPI.Modules
{
    public class SavePostResponse
    {
        public int PostId { get; set; }
        public string message { get; set; }
    }
}
