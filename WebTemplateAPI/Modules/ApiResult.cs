﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using WebTemplateAPI.Modules.Data;

namespace WebTemplateAPI.Modules
{
    public class ApiResult<T>
    {
        private ApiResult(List<Users> data, string? username, string? password)
        {
            Data = data;
            Username = username;
            Password = password;
        }

        public static async Task<ApiResult<T>> CheckLogin(IQueryable<Users> source, string? usrname = null, string? password=null)
        { 
            if(!string.IsNullOrEmpty(usrname) && !string.IsNullOrEmpty(password))
            {
                password = HashWithSHA256(password);
                source = source.Where(value => value.UserName == usrname && value.PasswordHash == password);
            }
            var data = await source.ToListAsync();
            var count = await source.CountAsync();
            if(count != 1)
            {
                throw new Exception("Too many users");
            }
            else
            {
                return new ApiResult<T>(data,usrname, password);
            }

        }
        public static string HashWithSHA256(string value)
        {
            using var hash = SHA256.Create();
            var byteArray = hash.ComputeHash(Encoding.UTF8.GetBytes(value));
            return Convert.ToHexString(byteArray);
        }
        public List<Users> Data { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
    }
}
