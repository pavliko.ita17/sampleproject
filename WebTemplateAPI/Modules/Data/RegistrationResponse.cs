﻿namespace WebTemplateAPI.Modules.Data
{
    public class RegistrationResponse
    {
        public bool IsSuccessfullRegistration { get; set; }
        public IEnumerable<string>? Errors { get; set; }
    }
}
