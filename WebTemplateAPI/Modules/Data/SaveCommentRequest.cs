﻿namespace WebTemplateAPI.Modules.Data
{
    public class SaveCommentRequest
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public int ParentId { get; set; } = 0;
        public int PostId { get; set; }

    }
}
