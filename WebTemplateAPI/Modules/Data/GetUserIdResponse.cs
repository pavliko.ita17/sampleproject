﻿namespace WebTemplateAPI.Modules.Data
{
    public class GetUserIdResponse
    {
        public string UserId { get; set; }
    }
}
