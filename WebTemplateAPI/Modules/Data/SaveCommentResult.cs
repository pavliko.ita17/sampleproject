﻿namespace WebTemplateAPI.Modules.Data
{
    public class SaveCommentResult
    {
        public string message { get; set; }
        public int commentId { get; set; }
    }
}
