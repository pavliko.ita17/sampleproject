﻿using Microsoft.Build.Framework;

namespace WebTemplateAPI.Modules.Data
{
    public class RegistrationRequest
    {
        public string? username { get; set; }
        public string? Email { get; set; }
        [Required]
        public string? Password { get; set; }
        [Required]
        public string? ConfirmPassword { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
