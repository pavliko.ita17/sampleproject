﻿namespace WebTemplateAPI.Modules.Data
{
    public class SavePostRequest
    {
        public string author { get; set; }
        public string content { get; set; }
    }
}
