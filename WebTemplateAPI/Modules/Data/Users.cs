﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebTemplateAPI.Modules.Data
{
    public class Users : IdentityUser
    {
        public ICollection<Posts>? Posts { get; set; }
        public ICollection<Comments>? Comments { get; set; }
    }
}
