﻿namespace WebTemplateAPI.Modules.Data
{
    public class CommentsResult
    {
        public int comment_id { get; set; }
        public string content { get; set; }
        public string author { get; set; }
    }
}
