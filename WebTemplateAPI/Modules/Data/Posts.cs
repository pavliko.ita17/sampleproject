﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace WebTemplateAPI.Modules.Data
{
    [Table("posts")]
    [Index(nameof(posts_id))]
    [Index(nameof(content))]
    [Index(nameof(created_at))]
    [Index(nameof(updated_at))]
    [Index(nameof(author))]
    [Index(nameof(banned))]
    public class Posts
    {
        [Key]
        [Required]
        [Column(TypeName ="decimal(10)")]
        public int posts_id { get; set; }
        [Column(TypeName ="longtext")]
        public string content { get; set; } = null!;
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        [ForeignKey(nameof(Users))]
        public string author { get; set; }
        public bool banned { get; set; }
        public Users? Users { get; set; }
        public ICollection<Comments>? Comments { get; set; }
    }
}
