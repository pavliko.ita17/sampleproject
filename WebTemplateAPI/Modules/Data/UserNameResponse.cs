﻿namespace WebTemplateAPI.Modules.Data
{
    public class UserNameResponse
    {
        public string UserName { get; set; }
    }
}
