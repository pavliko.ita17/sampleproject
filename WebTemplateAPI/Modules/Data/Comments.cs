﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebTemplateAPI.Modules.Data
{
    [Table("comments")]
    [Index(nameof(comm_id))]
    [Index(nameof(posts_id))]
    [Index(nameof(author))]
    [Index(nameof(parent_id))]
    [Index(nameof(content))]
    [Index(nameof(createdAt))]
    [Index(nameof(updatedAt))]
    public class Comments
    {
        [Key]
        [Required]
        [Column(TypeName = "decimal(10)")]
        public int comm_id { get; set; }
        [ForeignKey(nameof(Posts))]
        public int posts_id { get; set; }
        [ForeignKey(nameof(Users))]
        public string author { get; set; }
        [ForeignKey(nameof(Comments))]
        public int parent_id { get; set; } = 0;
        [Required]
        [Column(TypeName = "longtext")]
        public string content { get; set; } = null!;
        [Required]
        public DateTime createdAt { get; set; }
        [Required]
        public DateTime updatedAt { get; set; }
        public Posts? Posts { get; set; }
        public Users? Users { get; set; }
        public ICollection<Comments>? Parents { get; } = new List<Comments>();
    }
}
