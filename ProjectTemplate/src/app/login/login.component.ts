import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { BaseFormComponent } from '../base-form.component'  ;
import { CheckPasswd } from '../CheckPasswd';
import { CheckPasswdReq } from '../CheckPasswdReq';
import { AuthService } from '../guards/auth.service';
import { GetUserIdRequest } from './GetUserIdRequest';
import { LoginRequest } from './LoginRequest';
import { LoginResult } from './LoginResult';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseFormComponent implements OnInit {
  title?: string;
  loginResult?: LoginResult;
  err: string[] =[]
  constructor(private activatedRoute: ActivatedRoute, private routrer: Router, private authservice: AuthService){
    super();
  }
  ngOnInit(){
    this.form = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    });
  }
  onSubmit(){
    var LoginRequest = <LoginRequest>{};
    LoginRequest.UserName = this.form.controls['username'].value;
    LoginRequest.password = this.form.controls['password'].value;
    this.checkPsswd(LoginRequest.UserName,LoginRequest.password);
    var getUserId =<GetUserIdRequest>{};
    getUserId.username = this.form.controls['username'].value;
    this.authservice.login(LoginRequest).subscribe(result => {
      this.loginResult = result;
      console.log(this.loginResult);
      if(result.success&& result.token){
        localStorage.setItem(this.authservice.tokenKey, result.token);
        localStorage.setItem("userName",this.form.controls['username'].value);
        console.log(localStorage.getItem(this.authservice.tokenKey));
        console.log(localStorage.getItem("userName"));
        this.routrer.navigate(["/"]);

      }
    }, error => {
      console.log(error);
      if(error.status == 401){
        this.loginResult = error.error;
      }
    });
    this.authservice.getUserId(getUserId).subscribe(result =>{
        let json = JSON.parse(JSON.stringify(result));
        console.log(json);
        console.log(json.userId);
        localStorage.setItem("userId",json.userId);
        console.log(localStorage.getItem("userId"));
    },error => {
      console.log(error);
    });
  }
  checkPsswd(username: string,password: string){
    var badpasswd :string;
    var check = <CheckPasswdReq>{};
    var chekResp = <CheckPasswd> {};
    if(password != "" && username != ""){
      check.password =password;
      check.userName = username;
      this.authservice.checkPassword(check).subscribe(res => {
        chekResp.message = res.message;
        this.err.push(chekResp.message);
      })
      console.log(this.err);
    }
  }
}
