import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMateriaLModule } from './app-material.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './guards/auth.service';
import { BaseFormComponent } from './base-form.component';
import { RegisterComponent } from './register/register.component';
import { BlogComponent } from './blog/blog.component';
import { CommentComponent } from './blog/comment/comment.component';
import { PostComponent } from './blog/post/post.component';
import { AngularEditorModule,AngularEditorConfig } from '@kolkov/angular-editor';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommentstreeComponent } from './blog/comment/commentstree.component';
import {MatTreeModule} from '@angular/material/tree';
import { CommentNode } from './blog/comment/commentstree.component';
import { PostsComponent } from './blog/post/posts.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
export function tokenGetter() {
  return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FetchDataComponent,
    NavMenuComponent,
    LoginComponent,BaseFormComponent,RegisterComponent,BlogComponent,CommentComponent,PostComponent,CommentstreeComponent,PostsComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,AppRoutingModule, BrowserAnimationsModule,AppMateriaLModule,FormsModule,
    JwtModule.forRoot({config:{tokenGetter:tokenGetter,allowedDomains:["localhost:40433"],disallowedRoutes:[]}}),FormsModule,ReactiveFormsModule,AngularEditorModule, FontAwesomeModule,MatTreeModule,ScrollingModule
    ,InfiniteScrollModule
  ],
  exports:[
    CommentComponent
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
