import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { LoginRequest } from '../login/LoginRequest';
import { LoginResult } from '../login/LoginResult';
import { RegisterRequest } from '../register/RegisterRequest';
import { RegisterResponse } from '../register/RegisterResponse';
import { GetUserIdRequest } from '../login/GetUserIdRequest';
import { GetUserIdResponse } from '../login/GetUserIdResponse';
import { Comments } from '../blog/Comments';
import { PostLoads } from '../blog/post/PostLoad';
import { SavePostRequest } from '../blog/post/SavePostRequest';
import { SavePostResponse } from '../blog/post/SavePostResponse';
import { GetUserName } from '../blog/post/GetUserName';
import { SaveCommentRequest } from '../blog/comment/SaveCommentRequest';
import { SaveCommentResponse } from '../blog/comment/SaveCommentResponse';
import { CheckPasswd } from '../CheckPasswd';
import { CheckPasswdReq } from '../CheckPasswdReq';

@Injectable({
  providedIn: 'root'
})
export class AuthService   {
  constructor(private http:HttpClient){}
  public tokenKey : string = "token";

  isAuthenticated(): boolean{
    return this.getToken("token") !== null;
  }

  getToken(name?:string) : string | null {
    return localStorage.getItem(name!);
  }
  login(item: LoginRequest): Observable<LoginResult>{
    var url = environment.baseUrl + "api/Account/Login";
    return this.http.post<LoginResult>(url,item);
  }

  registerUser(item: RegisterRequest): Observable<RegisterResponse>{
    var url = environment.baseUrl + "api/Account/Registration"
    return this.http.post<RegisterResponse>(url,item);
  }
  getUserId(getUserId: GetUserIdRequest): Observable<GetUserIdResponse>{
    var url = environment.baseUrl + "api/Account/GetUId";
    return this.http.post<GetUserIdResponse>(url,getUserId);
  }
  getCommets(id: number):Observable<Comments[]>{
    var url = environment.baseUrl + "api/Comments/LoadComments?post_id=" + id;
    return this.http.get<Comments[]>(url);
  }
  getReplies(id: number): Observable<Comments[]>{
    var url = environment.baseUrl + "api/Comments/LoadReplies?parent_id=" + id;
    return this.http.get<Comments[]>(url);
  }
  getPosts(page: number,LastPostID: number | 0):Observable<PostLoads[]>{
    var url = environment.baseUrl + "api/Posts/LoadPosts?pageNumber=" + page + "&lastId=" + LastPostID;
    return this.http.get<PostLoads[]>(url);
  }
  getAuthor(author: string):Observable<GetUserName>{
    var url = environment.baseUrl + "api/Account/GetUserName?userId="+author;
    return this.http.get<GetUserName>(url);
  }
  savePost(save:SavePostRequest): Observable<SavePostResponse>{
    var url = environment.baseUrl + "api/Posts/SavePost"
    return this.http.post<SavePostResponse>(url,save);
  }
  saveComment(save: SaveCommentRequest): Observable<SaveCommentResponse>{
    var url = environment.baseUrl + "api/Comments/SaveComment"
    return this.http.post<SaveCommentResponse>(url,save);
  }
  checkPassword(check: CheckPasswdReq): Observable<CheckPasswd>{
    var url = environment.baseUrl + "api/Account/CheckPassword";
    return this.http.post<CheckPasswd>(url,check);

  }
}
