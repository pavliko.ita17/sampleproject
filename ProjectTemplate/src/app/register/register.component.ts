import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { BaseFormComponent } from '../base-form.component';
import { AuthService } from '../guards/auth.service';
import { RegisterRequest } from './RegisterRequest';
import { RegisterResponse } from './RegisterResponse';
import { ConfirmPasswordValidator } from './confirm-password.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends BaseFormComponent implements OnInit {
  title?: string;
  registerResult?: RegisterResponse;
  constructor(private activatedRoute: ActivatedRoute, private routrer: Router, private authservice: AuthService,private fb:FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: new FormControl('',Validators.required),
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required),
      confirm: new FormControl('',Validators.required),
      phonenumber: new FormControl('',Validators.required)
    },
    {
      validator: ConfirmPasswordValidator("password", "confirm")
    });
  }
  get passwordMatchError(){
    return(this.form.getError('mismatch')&& this.form.get('confirm')?.touched);
  }
  onSubmit(){
    var RegisterRequest = <RegisterRequest>{};
    RegisterRequest.username = this.form.controls['username'].value;
    RegisterRequest.email = this.form.controls['email'].value;
    RegisterRequest.password = this.form.controls['password'].value;
    RegisterRequest.confirmPassword = this.form.controls['confirm'].value;
    RegisterRequest.PhoneNubmer = this.form.controls['phonenumber'].value;

    this.authservice.registerUser(RegisterRequest).subscribe(result =>{
      console.log(result);
      this.registerResult = result;

      this.routrer.navigate(['login']);


    },error => {
      console.log(error);
      if(error.status == 401){
        this.registerResult = error.error;
      }
    });

  }
}
