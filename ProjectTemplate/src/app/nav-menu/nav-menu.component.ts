import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from '../guards/auth.service';
import { LoginResult } from '../login/LoginResult';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  authservic: AuthService;
  loggedUser: string = localStorage.getItem("userName")!;

  constructor(private jwtHelper : JwtHelperService,authservice: AuthService){
    this.authservic = authservice;
  }
  ngOnInit(): void {
  }
  isUserAuthenticated = (): boolean => {
    const token = localStorage.getItem(this.authservic.tokenKey)
    this.loggedUser = localStorage.getItem("userName")!;
    if(token && !this.jwtHelper.isTokenExpired(token)){
      return true;
    }
    return false;
  }

  logout = () =>{
    localStorage.removeItem(this.authservic.tokenKey);

  }
}
