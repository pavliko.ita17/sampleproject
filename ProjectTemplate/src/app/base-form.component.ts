import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, ValidatorFn,ValidationErrors } from '@angular/forms';
import { CheckPasswdReq } from './CheckPasswdReq';
import { AuthService } from './guards/auth.service';

@Component({
  template: ''
})
export class BaseFormComponent{
  form!: FormGroup;
  authService!: AuthService;
  constructor() {

  }
  getErrors(control:AbstractControl, displayName: string, matchingControl?: AbstractControl, userName?: AbstractControl): string[]{
    var errors: string[] = [];


    if(matchingControl != undefined && control.value != matchingControl.value){
      errors.push(`Hesla se neshodují`)
    }
    else{
    Object.keys(control.errors || {}).forEach((key)=> {
      switch(key){
        case 'required':
          errors.push(`${displayName} nebylo vyplněno.`);
          break;
        case 'pattern':
          errors.push(`${displayName} obsahuje neplatný text.`);
          break;
        default:
          errors.push(`${displayName} je neplatné.`);
          break;
      }
    })
    }
    return errors;

  }
  static MatchValidator(source: string, target: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const sourceCtrl = control.get(source);
      const targetCtrl = control.get(target);

      return sourceCtrl && targetCtrl && sourceCtrl.value !== targetCtrl.value
        ? { mismatch: true }
        : null;
    };
  }


}
