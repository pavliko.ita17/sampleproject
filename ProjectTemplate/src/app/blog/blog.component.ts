import { Component, OnInit, ViewChild } from '@angular/core';
import { CommentstreeComponent } from './comment/commentstree.component';
import { AuthService } from '../guards/auth.service';
import { CommentNode } from './comment/commentstree.component';
import { PostNode } from './post/post.component';
import { PostLoads } from './post/PostLoad';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  comments:Array<CommentNode> = [];
  posts: Array<PostNode> = [];
  autor: string = ""
  page: number = 1;
  usernam : string = "";
  PostToLoad: PostLoads[] = [];
  constructor(private service: AuthService){

  }
  ngOnInit(): void {
   // this.getNextPosts();
  }

}
