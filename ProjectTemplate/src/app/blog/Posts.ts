export interface Posts{
  post_id: number;
  content: string;
  created_at: Date;
  author: string;
  banned: boolean;
}
