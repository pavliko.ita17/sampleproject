import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentstreeComponent } from './commentstree.component';

describe('CommentstreeComponent', () => {
  let component: CommentstreeComponent;
  let fixture: ComponentFixture<CommentstreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentstreeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CommentstreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
