import { Component, OnInit,Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from 'src/app/base-form.component';
import { AuthService } from 'src/app/guards/auth.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import { CommentNode } from './commentstree.component';
import { BlogComponent } from '../blog.component';
import { Comments } from '../Comments';
import { SaveCommentRequest } from './SaveCommentRequest';
import { SaveCommentResponse } from './SaveCommentResponse';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent extends BaseFormComponent implements OnInit {
  loggedUser = localStorage.getItem("userName");
  @Input() public comments:CommentNode[] = [];
  @Input() public post_Id: number = 0;
  commentsShown: boolean = false;
  loadComs : Comments[] = [];
  constructor(private activatedRoute: ActivatedRoute, private routrer: Router, private authservice: AuthService) {
    super();
  }
  isOpen:boolean = false;
  comment_id : number = 0;
  commentConfig: AngularEditorConfig ={
    editable: true,
    width: '35em',
    defaultFontName: 'Times New Roman',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ], toolbarHiddenButtons: [
      ['insertImage','insertVideo','textColor',
      'backgroundColor']
    ]
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      content: new FormControl('',Validators.required)
    })
  }
  OnSubmit(){
    console.log(this.form.controls["content"].value)
  }
  openCommentText(){
    this.isOpen = true;
    if(!this.commentsShown){
      this.showComments();
    }
  }
  showComments(){
    this.authservice.getCommets(this.post_Id).subscribe(res=>{
      this.loadComs = res;
      this.loadComs.forEach(data=>{
        this.authservice.getAuthor(data.author).subscribe(authres=>{
          console.log(data.comm_id);
          this.comments.push(new CommentNode(data.content,authres.userName,data.comm_id,0,this.post_Id));
        })
      })
    })
    this.commentsShown = true;
  }
  addComment(){
      console.log(this.post_Id);
      var savecom = <SaveCommentRequest>{};
      var savecomres = <SaveCommentResponse>{};
      savecom.content = this.form.controls['content'].value;
      savecom.author = localStorage.getItem('userId')!;
      savecom.ParentId = 0;
      savecom.PostId = this.post_Id;
      console.log(savecom.PostId);
      this.authservice.saveComment(savecom).subscribe(res=>{
        savecomres.commentId = res.commentId;
        savecomres.message = res.message;
        if(savecomres.message == "ok"){
          this.comments.push(new CommentNode(savecom.content,this.loggedUser!,savecomres.commentId,savecom.ParentId,savecom.PostId))
          console.log(this.comments)
          this.isOpen = false;
          this.form.reset();
        }
      },error=>{
        console.log(error);
      });

  }
}
