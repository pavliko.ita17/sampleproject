export interface SaveCommentResponse{
  message :string;
  commentId : number;
}
