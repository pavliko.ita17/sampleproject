import {SelectionModel} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import {Component, Injectable, Input, OnInit} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import { Data } from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import { BaseFormComponent } from 'src/app/base-form.component';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from 'src/app/guards/auth.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormGroup,FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { SaveCommentRequest } from './SaveCommentRequest';
import { SaveCommentResponse } from './SaveCommentResponse';
import { Comments } from '../Comments';

export class CommentNode {
  text:string ='';
  author: string = '';
  commentId: number = 0;
  anwsers:CommentNode[] = [];
  isOpen!:false;
  IsReplyShown :boolean= false;
  postId: number = 0;
  parent_Id : number= 0;
  constructor(text:string,author: string, commentId :number | 0, parent_Id: number | 0, postId : number | 0){
    this.author = author;
    this.commentId = commentId;
    this.text = text;
    this.postId = postId;
    this.parent_Id = parent_Id;
  }

  addAnwser(newComment:CommentNode){
    if(newComment.text){
      this.anwsers.push(newComment);
    }
  }


  removeComment(newComment:CommentNode){
    let index = this.anwsers.indexOf(newComment);
    if(~index){
      this.anwsers.slice(index,1);
    }
  }
}
@Component({
  selector: 'app-commentstree',
  templateUrl: 'commentstree.component.html',
  styleUrls: ['commentstree.component.css'],

})
export class CommentstreeComponent extends BaseFormComponent implements OnInit {
  @Input()
  public comments:CommentNode[] = [];
  @Input()
  public post_Id: number = 0;
  @Input()
  public parent_Id: number = 0;
  postfther: number = this.parent_Id;
  text:string = '';
  loadRepls: Comments[] = []
  comment_id: number = 0;
  isOpen = false;
  isReplyShown = false;
  showRepl:boolean = false;
  constructor(private activatedRoute: ActivatedRoute, private routrer: Router, private authservice: AuthService){ super();}
  loggedUser = localStorage.getItem("userName");
  ngOnInit(){
    console.log(this.comments);
    this.form = new FormGroup({
      content: new FormControl('',Validators.required)
    })
  }
  commentConfig: AngularEditorConfig ={
    editable: true,
    width: '30em',
    defaultFontName: 'Times New Roman',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ], toolbarHiddenButtons: [
      ['insertImage','insertVideo','textColor',
      'backgroundColor']
    ]
  }

  addComment(comment:CommentNode){
    console.log(this.parent_Id);
    var savecom = <SaveCommentRequest>{};
    var savecomres = <SaveCommentResponse>{};
    savecom.content = this.form.controls['content'].value;
    savecom.author = localStorage.getItem('userId')!;
    savecom.ParentId = comment.commentId;
    savecom.PostId = this.post_Id;
    this.authservice.saveComment(savecom).subscribe(res=>{
      savecomres.commentId = res.commentId;
      savecomres.message = res.message;
      if(savecomres.message == "ok"){
        comment.addAnwser(new CommentNode(savecom.content,this.loggedUser!,savecomres.commentId,savecom.ParentId,savecom.PostId))
        comment.isOpen = false;
        this.showRepl = true;
        this.text="";
        this.comment_id=0;
        console.log(this.comments);
        this.form.reset();
      }
    },error=>{
      console.log(error);
    });
  }

  openCommentText(comment:any){
      console.log(comment.commentId);
      this.comment_id = comment.commentId;
      console.log(comment)
      comment.isOpen = !comment.isOpen;
      if(!comment.isReplyShown){
        this.showReplies(comment);
      }
  }

  remove(comment:CommentNode){
    let index = this.comments.indexOf(comment);
    this.comments = this.comments.splice(index,1);
  }

  response: any;
  showReplies(comment: CommentNode){
    this.authservice.getReplies(comment.commentId).subscribe(result=>{
      this.loadRepls = result;
      this.loadRepls.forEach(data=> {
        this.authservice.getAuthor(data.author).subscribe(authres=>{
          comment.addAnwser(new CommentNode(data.content,authres.userName,data.comm_id,this.parent_Id,this.post_Id))
        })
      })
      });
      this.showRepl = true;
      comment.IsReplyShown = !comment.IsReplyShown;
  }
  get FiltredByPosts(){
    return this.comments.filter(x=> x.postId==this.post_Id);
  }
}
