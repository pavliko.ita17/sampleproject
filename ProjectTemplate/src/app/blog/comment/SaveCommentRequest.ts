export interface SaveCommentRequest{
  content: string;
  author: string;
  ParentId : number;
  PostId: number;
}
