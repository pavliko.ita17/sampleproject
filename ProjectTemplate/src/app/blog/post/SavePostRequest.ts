export interface SavePostRequest{
  author: string;
  content: string;
}
