export interface SavePostResponse{
  message: string;
  postId : number;
}
