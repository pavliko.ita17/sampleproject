export interface PostLoads{
  posts_id :number;
  content: string;
  created_at: Date;
  updated_at: Date;
  author: string;
  banned: boolean;
}
