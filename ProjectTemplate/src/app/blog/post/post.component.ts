import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from 'src/app/base-form.component';
import { AuthService } from 'src/app/guards/auth.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SavePostResponse } from './SavePostResponse';
import { SavePostRequest } from './SavePostRequest';
import { formatDate } from '@angular/common';
export class PostNode{
  post: PostNode[] = [];
  content : string = '';
  postId : number = 0;
  author: string = '';
  createdAt! : Date;
  constructor(content:string,author:string,createdAt: Date, postId : number | 0){
    this.content = content;
    this.author = author;
    this.createdAt = createdAt;
    this.postId = postId;
  }

  createPost(){
    this.post.push(new PostNode(this.content,this.author,this.createdAt,this.postId));
  }
}
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent extends BaseFormComponent implements OnInit {
  @Input()
  public posts: PostNode[] = [] ;
  loggedUser = localStorage.getItem("userName");
  Uid? = localStorage.getItem("userId");
  isOpen : boolean = false;
  Crdate :Date = new Date()
  constructor(private activatedRoute: ActivatedRoute, private routrer: Router, private authservice: AuthService) { super(); }
  postConfig: AngularEditorConfig ={
    editable: true,
    width: '40em',
    defaultFontName: 'Times New Roman',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ], toolbarHiddenButtons: [
      ['insertImage','insertVideo','textColor',
      'backgroundColor']
    ]
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      content: new FormControl('',Validators.required)
    })
  }
  showPostEdit(){
    this.isOpen = true;
  }
  createPost(){
    var SaveReq = <SavePostRequest>{};
    var SaveRespo = <SavePostResponse>{};
    console.log(this.Uid);
   SaveReq.author = this.Uid!;
   SaveReq.content = this.form.controls['content'].value;
   this.authservice.savePost(SaveReq).subscribe(res=>{
    SaveRespo.message = res.message;
    SaveRespo.postId = res.postId;
    console.log(SaveRespo.message);
    if(SaveRespo.message == "ok"){
      this.posts.push(new PostNode(this.form.controls['content'].value,this.loggedUser!,this.Crdate,SaveRespo.postId));
      this.isOpen = false;
    }
   },err => {
    console.log(err);
   });

  }
}
