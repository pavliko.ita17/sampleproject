import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/guards/auth.service';
import { CommentNode } from '../comment/commentstree.component';
import { PostNode } from './post.component';
import { PostLoads } from './PostLoad';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  @Input() public posts: PostNode[] = [];
  @Input() public comments: CommentNode[] = [];
  page: number = 6;
  PostToLoad: PostLoads[] = [];
  autor: string = "";
  usernam: string ="";
  ngOnInit(): void {
    this.getNextPosts(0);
  }
  constructor(private service: AuthService){}
  getNextPosts(LastPostID: number | 0){
    this.service.getPosts(this.page,LastPostID).subscribe((res)=>{
      this.PostToLoad = res;
      this.PostToLoad.forEach(psts=>{
        console.log(psts.author);
        this.service.getAuthor(psts.author).subscribe(res=>{
          this.posts.push(new PostNode(psts.content,res.userName,psts.created_at,psts.posts_id));
        },error=>{
          console.log(error);
        })
      })
    },error=>{
      console.log(error);
    })

  }
  OnScroll(){
    this.page++;
    console.log(this.page);
    this.posts.sort((a,b)=>a.postId-b.postId)
    var last = this.posts[this.posts.length-1];
    if(last.postId > this.page){
      this.getNextPosts(last.postId);
    }
  }

}
