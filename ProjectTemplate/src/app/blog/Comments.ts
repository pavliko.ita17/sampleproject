export interface Comments{
  comm_id: number;
  author: string;
  content: string;
}
