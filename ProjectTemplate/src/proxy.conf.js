const PROXY_CONFIG = [
  {
    context: [
      "/",
    ],
    target: "https://localhost:40433",
    secure: false
  }
]

module.exports = PROXY_CONFIG;
